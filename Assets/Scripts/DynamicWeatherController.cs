﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Maelstrom.RPG
{
    [RequireComponent(typeof(Light))]
    [RequireComponent (typeof(AudioSource))]
    public class DynamicWeatherController : MonoBehaviour
    {
        #region Variables
        public enum WeatherStates
        {
            Pick,
            Sunny,
            Overcast,
            Rain,
            Thunder,
            Fog,
            Snow
        }

        public Transform target;
        public float weatherHeight;

        public Text weatherText;

        //Weather variables
        private WeatherStates _weatherState;
        private int _switchWeather;

        public float minWeatherTime;
        public float maxWeatherTime;
        private float _switchWeatherTimer = 0f;   //switch weather time equals zero
        private float _resetWeatherTimer;

        public Weather sunnyWeather;
        public Weather overcastWeather;
        public Weather rainWeather;
        public Weather thunderWeather;
        public Weather fogWeather;
        public Weather snowWeather;

        //Audio control
        private AudioSource audioSource;
        public float audioFadeTime = 0.25f;

        //Ambient Light variables
        public float lightTransitionTime = 0.1f;   //defines rate for light change
        public float fogChangeSpeed = 0.1f;

        //Skyboxes
        public float skyboxBlendValue;
        public float skyboxBlendTime = 0.25f;  //way too fast.  kept as is for testing

        #endregion



        #region Main Methods    

        void Start()
        {
            EnableParticleSystems();

            audioSource = GetComponent<AudioSource>();

            if (!target)
            {
                GameObject player = GameObject.FindGameObjectWithTag("Player");
                target = player.transform;
            }

            RenderSettings.fog = true;
            RenderSettings.fogMode = FogMode.ExponentialSquared;
            RenderSettings.fogDensity = 0.02f; //0.01-0.02f

            RenderSettings.skybox.SetFloat("_Blend", 0);

            StartCoroutine(WeatherFSM());
        }

        void Update()
        {
            SwitchWeatherTimer(); //updates timer function
        }

        void LateUpdate()
        {
            transform.position = new Vector3(target.position.x, target.position.y + weatherHeight, target.position.z);
        }

        void SwitchWeatherTimer()
        {
            _switchWeatherTimer -= Time.deltaTime;

            if (_switchWeatherTimer > 0)
            {
                return;
            }
            else
            {
                _weatherState = WeatherStates.Pick;
                _resetWeatherTimer = UnityEngine.Random.Range(minWeatherTime, maxWeatherTime);
                _switchWeatherTimer = _resetWeatherTimer;
            }
        }
        #endregion

        #region Weather Methods

        IEnumerator WeatherFSM()
        {
            while (true)  //while the weather state machine is active switch the weather
            {
                switch (_weatherState)
                {
                    case WeatherStates.Pick:
                        {
                            PickWeather();
                            break;
                        }
                    case WeatherStates.Sunny:
                        {
                            SunnyWeather();
                            break;
                        }
                    case WeatherStates.Overcast:
                        {
                            OvercastWeather();
                            break;
                        }
                    case WeatherStates.Rain:
                        {
                            RainWeather();
                            break;
                        }
                    case WeatherStates.Thunder:
                        {
                            ThunderWeather();
                            break;
                        }
                    case WeatherStates.Fog:
                        {
                            FogWeather();
                            break;
                        }
                    case WeatherStates.Snow:
                        {
                            SnowWeather();
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }

                yield return null;
            }
        }

        void PickWeather()
        {
            DisableParticleSystems();

            _switchWeather = UnityEngine.Random.Range(0, 6); //upper bound is number of weather states

            switch (_switchWeather)
            {
                case 0:
                    _weatherState = WeatherStates.Sunny;
                    break;
                case 1:
                    _weatherState = WeatherStates.Overcast;
                    break;
                case 2:
                    _weatherState = WeatherStates.Rain;
                    break;
                case 3:
                    _weatherState = WeatherStates.Thunder;
                    break;
                case 4:
                    _weatherState = WeatherStates.Fog;
                    break;
                case 5:
                    _weatherState = WeatherStates.Snow;
                    break;
            }
        }

        void SunnyWeather()
        {
            weatherText.text = "Sunny";
            sunnyWeather.particleSystem.Play();

            AdjustLightIntensity(sunnyWeather.ambientLightIntensity);
            CrossFadeAudio(sunnyWeather.audioClip);

            Color _currentFogColor = RenderSettings.fogColor;
            RenderSettings.fogColor = Color.Lerp(_currentFogColor, sunnyWeather.fogColor, fogChangeSpeed * Time.deltaTime);
        }

        void OvercastWeather()
        {
            weatherText.text = "Overcast";
            overcastWeather.particleSystem.Play();

            AdjustLightIntensity(overcastWeather.ambientLightIntensity);
            CrossFadeAudio(overcastWeather.audioClip);

            Color _currentFogColor = RenderSettings.fogColor;
            RenderSettings.fogColor = Color.Lerp(_currentFogColor, overcastWeather.fogColor, fogChangeSpeed * Time.deltaTime);
        }

        void RainWeather()
        {
            weatherText.text = "Rain";
            rainWeather.particleSystem.Play();

            AdjustLightIntensity(rainWeather.ambientLightIntensity);
            CrossFadeAudio(rainWeather.audioClip);

            Color _currentFogColor = RenderSettings.fogColor;
            RenderSettings.fogColor = Color.Lerp(_currentFogColor, rainWeather.fogColor, fogChangeSpeed * Time.deltaTime);
        }

        void ThunderWeather()
        {
            weatherText.text = "Thunder";
            thunderWeather.particleSystem.Play();

            AdjustLightIntensity(thunderWeather.ambientLightIntensity);
            CrossFadeAudio(thunderWeather.audioClip);

            Color _currentFogColor = RenderSettings.fogColor;
            RenderSettings.fogColor = Color.Lerp(_currentFogColor, thunderWeather.fogColor, fogChangeSpeed * Time.deltaTime);
        }

        void FogWeather()
        {
            weatherText.text = "Fog";
            fogWeather.particleSystem.Play();

            AdjustLightIntensity(fogWeather.ambientLightIntensity);
            CrossFadeAudio(fogWeather.audioClip);

            Color _currentFogColor = RenderSettings.fogColor;
            RenderSettings.fogColor = Color.Lerp(_currentFogColor, fogWeather.fogColor, fogChangeSpeed * Time.deltaTime);
        }

        void SnowWeather()
        {
            weatherText.text = "Snow";
            snowWeather.particleSystem.Play();

            AdjustLightIntensity(snowWeather.ambientLightIntensity);
            CrossFadeAudio(snowWeather.audioClip);

            Color _currentFogColor = RenderSettings.fogColor;
            RenderSettings.fogColor = Color.Lerp(_currentFogColor, snowWeather.fogColor, fogChangeSpeed * Time.deltaTime);
        }

        #endregion


        #region Helper Methods

        void EnableParticleSystems()
        {
            ParticleSystem[] systems = GetComponentsInChildren<ParticleSystem>(true);
            foreach (ParticleSystem ps in systems)
                ps.gameObject.SetActive(true);
        }

        void DisableParticleSystems()
        {
            sunnyWeather.particleSystem.Stop();
            overcastWeather.particleSystem.Stop();
            rainWeather.particleSystem.Stop();
            thunderWeather.particleSystem.Stop();
            fogWeather.particleSystem.Stop();
            snowWeather.particleSystem.Stop();
        }

        void AdjustLightIntensity(float lightIntensity)
        {
            Light ambientLight = GetComponent<Light>();
            if (ambientLight.intensity > lightIntensity)
            {
                ambientLight.intensity -= lightTransitionTime * Time.deltaTime;
            }

            else if (ambientLight.intensity < lightIntensity)
            {
                ambientLight.intensity += lightTransitionTime * Time.deltaTime;
            }
        }

        //functions as stop-play, not true crossfade
        void CrossFadeAudio(AudioClip clip)
        {
            //fade out the previous clip, if it is playing
            if (audioSource.volume > 0 && audioSource.clip != clip)
            {
                audioSource.volume -= audioFadeTime * Time.deltaTime;
            }

            if (clip != null)
            {              
                //change clips
                if (audioSource.volume == 0) 
                {
                    audioSource.Stop();
                    audioSource.clip = clip;
                    audioSource.loop = true;
                    audioSource.Play();
                }

                //fade in the current clip, if it is playing
                if (audioSource.volume < 1 && audioSource.clip == clip)
                {
                    audioSource.volume += audioFadeTime * Time.deltaTime;
                }
            }
        }

        #endregion

        //
        void SkyboxBlendManager()
        {
            Debug.Log("SkyboxBlendManager");

            skyboxBlendValue -= skyboxBlendTime * Time.deltaTime;
            RenderSettings.skybox.SetFloat("_Blend", skyboxBlendValue);

            //RenderSettings.skybox = SkyBoxes[(int)Weather.RAIN];
            //DynamicGI.UpdateEnvironment();
        }
    }

    [Serializable]
    public class Weather
    {
        public ParticleSystem particleSystem;
        public AudioClip audioClip;
        public float ambientLightIntensity;
        public Color fogColor;
    }

}

