﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;   

[ExecuteInEditMode]
public class TimeOfDay : MonoBehaviour {

    const int SECONDS_PER_DAY = 86400;

    [Range(0,SECONDS_PER_DAY)]
    public float time;
    public TimeSpan currentTime;
    public Transform sunTransform;
    public Light sun;
    public Text timeText;
    public int days;

    public float sunriseTime;
    public float sunsetTime;

    public float intensity;
    public Color fogDay = Color.grey;
    public Color fogNight = Color.black;

    public int speed = 1;
    public float rotationSpeed;

    public Material skybox;

    public float skyPhase = 0;
	
	void Update () {
        //transform.RotateAround(Vector3.zero, Vector3.right, rotationSpeed * Time.deltaTime);
        //transform.LookAt(Vector3.zero);

        ChangeTime();
        UpdateSky();
	}

    public void ChangeTime()
    {
        time += speed * Time.deltaTime;
        if (time > SECONDS_PER_DAY)  
        {
            days++;
            time = 0;
        }

        currentTime = TimeSpan.FromSeconds(time);
        string displayTime = string.Format("{0:D}:{1:D2}",
                currentTime.Hours,
                currentTime.Minutes);

        timeText.text = displayTime;

        sunTransform.rotation = Quaternion.Euler(new Vector3((time - SECONDS_PER_DAY/4)/SECONDS_PER_DAY * 360, 0, 0));

        //not sure if I like this....
        if (time < SECONDS_PER_DAY / 2)
        {
            intensity = 1 - ((SECONDS_PER_DAY / 2) - time) / (SECONDS_PER_DAY / 2);
        }
        else
        {
            intensity = 1 + ((SECONDS_PER_DAY / 2) - time) / (SECONDS_PER_DAY / 2);
        }

        //RenderSettings.fogColor = Color.Lerp(fogNight, fogDay, intensity * intensity);
        sun.intensity = intensity;

    }

    public void UpdateSky()
    {
        //night sky should be between sunsetTime and sunriseTime   or 72000 and 93600


        if (time > sunriseTime - 3600 && time < sunriseTime + 3600)
        {
            skyPhase = (time - (sunriseTime - 3600)) / 7200;
        }
        else if (time > sunsetTime - 3600 && time < sunsetTime + 3600)
        {
            skyPhase = 1 - ((time - (sunsetTime - 3600)) / 7200);
        }
        else if (time > sunsetTime || time < sunriseTime)
            skyPhase = 0;
        else
            skyPhase = 1;


        skybox.SetFloat("_SkyAlpha", skyPhase);
    }

}
