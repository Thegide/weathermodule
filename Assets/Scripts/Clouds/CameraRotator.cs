﻿using UnityEngine;

namespace Maelstrom.RPG
{
    
    //joystick-based camera rotator script
    public class CameraRotator : MonoBehaviour
    {
        #region Variables
        public Transform target;
        public float camSpeed = 2f;
        public float followDistance = 5f;

        private float smoothing;
        float maxHeight = 10f;
        float angle = 0f;
        float inputCamHorizontal = 0f;
        float inputCamVertical = 0f;
        float inputCamDistance = 0f;
        #endregion

        void Start()
        {
            Vector3 offset = target.transform.position - transform.position;
            followDistance = offset.magnitude;
        }

        void Update()
        {
            inputCamHorizontal = Input.GetAxisRaw("Horizontal R");
            inputCamVertical = Input.GetAxisRaw("Vertical R");

            transform.RotateAround(transform.position, Vector3.up, inputCamHorizontal * camSpeed * Time.deltaTime * 100f);
            transform.RotateAround(transform.position, transform.right, inputCamVertical * camSpeed * Time.deltaTime * 100f);
        }
    }

}