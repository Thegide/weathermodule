﻿using System;
using UnityEngine;
using UnityEditor;

public class VolumetricClouds : MonoBehaviour
{
    public Shader cloudShader;    
    [SerializeField]
    private Material cloudMaterial;
    
    [Header("World Settings")]    
    public float earthRadius = 6000f;
    public float cloudStartHeight = 200f;
    public float cloudEndHeight = 400f;
    private int cloudLayer = 0;
    private Vector3 earthCentre;

    [Header("Artistic Controls")]    
    [Range(0.001f, 2f)]
    public float gain = 0.13f;    
    [Range(0.001f, 1.0f)]
    public float coverage = 1f;
    [Range(0.02f, 2f)]
    public float scale = 0.1f;
    [Range(0.0f, 0.5f)]
    public float erosionAmount = 0.2f;
    [Range(0.001f, 2f)]
    public float erosionGain = 0.13f;
    [Range(0.1f, 10f)]
    public float erosionScale = 1f;
    [Range(0.1f,10f)]
    public float weatherScale = 1f;
    public Gradient stratus, stratocumulus, cumulus;
    private Vector4 stratusGradients, stratocumulusGradients, cumulusGradients;

    [Range(0.01f, 2f)]
    public float absorption = 0.01f;
    [Range(0.05f, 2.0f)]
    public float density = 1.0f;
    //[Range(0.0f, 0.9f)]
    public float powder = 0.5f;
    public Vector3 scattering = new Vector3(0.5f, 0.7f, 1.0f);

    [Header("Environment Controls")]
    public Vector3 windDirection = new Vector3(0f, 0f, 0f);
    public float windSpeed = 50f;
    public float windSkewFactor = 500f;

    [Header("Technical Controls")]
    [Range(1, 256)]
    public int raymarchSteps = 128;
    public float lightStepSize = 1f;

    public bool cloudBaseShape = true;
    public bool cloudDetailShape = true;
    public bool lightingEnabled = true;
    public bool fog = true;
    
    [Header("Viewport Info")]    
    public float camMoveSpeed = 1f;
    //Vector3 viewDirection;
    //Vector3 innerAtmosphereHit;
    //Vector3 outerAtmosphereHit;
    //float atmosphereLength;   
    private float heightFraction;
    private float camHeight;
    private Transform cam;
    private Vector2 screenSize;

    void Awake()
    {
        cam = Camera.main.transform;
        earthCentre = new Vector3(0f, -earthRadius, 0f);
    }

    void Update()
    {
        screenSize = new Vector2(Screen.width, Screen.height);

        if (cloudMaterial == null)
        {
            cloudMaterial = new Material(cloudShader);
            LoadCloudTextures();
        }

        if (Input.GetKey(KeyCode.W))
            cam.Translate(Vector3.up * camMoveSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.S))
            cam.Translate(-Vector3.up * camMoveSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.A))
            cam.Translate(-Vector3.right * camMoveSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.D))
            cam.Translate(Vector3.right * camMoveSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.UpArrow))
            cam.Translate(Vector3.forward * 5 * camMoveSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.DownArrow))
            cam.Translate(-Vector3.forward * 5 * camMoveSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.RightArrow))
            cam.Rotate(Vector3.up * 5 * camMoveSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.LeftArrow))
            cam.Rotate(-Vector3.up * 5 * camMoveSpeed * Time.deltaTime);

        Debug.DrawRay(cam.position, cam.forward, Color.red);
        earthCentre.y = -earthRadius;

        camHeight = (cam.position - earthCentre).magnitude;
        if (camHeight < cloudStartHeight + earthRadius) { cloudLayer = 0; }
        else if (camHeight > cloudEndHeight + earthRadius) { cloudLayer = 2; }
        else { cloudLayer = 1; }

        //viewDirection = Vector3.Normalize(cam.forward);
        //innerAtmosphereHit = RaySphereIntersect(viewDirection, cam.position, cloudStartHeight);
        //outerAtmosphereHit = RaySphereIntersect(viewDirection, cam.position, cloudEndHeight);
        //atmosphereLength = Vector3.Magnitude(outerAtmosphereHit - innerAtmosphereHit);        
        //heightFraction = HeightFraction(camHeight);

        UpdateGradients();
        SetShaderParams();
    }
        
    Vector3 RaySphereIntersect(Vector3 ray, Vector3 origin, float height)
    {
        Vector3 startPos = origin-earthCentre;

        float radius = earthRadius + height;

        float a = Vector3.Dot(ray, ray);
        float b = 2f * Vector3.Dot(ray, startPos);
        float c = Vector3.Dot(startPos, startPos) - (radius * radius);

        float discr = b * b - 4 * a * c;

        float t = Mathf.Max(0, (-b + Mathf.Sqrt(discr)) / (2f * a));

        return origin + ray * t;
    }

    float HeightFraction(float height)
    {        
        float startHeight = cloudStartHeight - earthCentre.y;
        float endHeight = cloudEndHeight - earthCentre.y;

        return Remap(height, startHeight, endHeight, 0.0f, 1.0f);
    }

    float Remap(float value, float original_min, float original_max, float new_min, float new_max)
    { 
        return new_min + (((value - original_min) / (original_max - original_min)) * (new_max - new_min));
    }

    void LoadCloudTextures()
    {
        Texture2D _weather = Resources.Load<Texture2D>("Weather");
        Texture3D _cloudShape = Resources.Load<Texture3D>("CloudShape");
        Texture3D _cloudDetail = Resources.Load<Texture3D>("CloudDetail");
        Texture2D _curlNoise = Resources.Load<Texture2D>("CurlNoise");

        if (_cloudShape == null)
            Debug.LogError("Error loading CloudShape.asset.  File not found");

        if (_cloudDetail == null)
            Debug.LogError("Error loading CloudDetail.asset.  File not found");

        cloudMaterial.SetTexture("_WeatherTex", _weather);
        cloudMaterial.SetTexture("_CloudTex", _cloudShape);
        cloudMaterial.SetTexture("_CloudDetail", _cloudDetail);
        cloudMaterial.SetTexture("_CurlNoise", _curlNoise);
    }

    void UpdateGradients()
    {
        GradientColorKey[] gckStratus = stratus.colorKeys;
        GradientColorKey[] gckStratocumulus = stratocumulus.colorKeys;
        GradientColorKey[] gckCumulus = cumulus.colorKeys;

        stratusGradients = new Vector4(gckStratus[0].time, gckStratus[1].time, gckStratus[2].time, gckStratus[3].time);
        stratocumulusGradients = new Vector4(gckStratocumulus[0].time, gckStratocumulus[1].time, gckStratocumulus[2].time, gckStratocumulus[3].time);
        cumulusGradients = new Vector4(gckCumulus[0].time, gckCumulus[1].time, gckCumulus[2].time, gckCumulus[3].time);
    }

    void SetShaderParams()
    {
        cloudMaterial.SetInt("_Frame", Time.frameCount);
        cloudMaterial.SetVector("_ScreenSize", screenSize);

        cloudMaterial.SetFloat("_EarthRadius", earthRadius);
        cloudMaterial.SetFloat("_CloudStartHeight", cloudStartHeight);
        cloudMaterial.SetFloat("_CloudEndHeight", cloudEndHeight);

        cloudMaterial.SetInt("_Shaping", cloudBaseShape ? 1 : 0);
        cloudMaterial.SetInt("_Details", cloudDetailShape ? 1 : 0);
        cloudMaterial.SetInt("_Lighting", lightingEnabled ? 1 : 0);
        cloudMaterial.SetInt("_Fog", fog ? 1 : 0);

        cloudMaterial.SetInt("_Steps", raymarchSteps);
        cloudMaterial.SetFloat("_ShapeGain", gain);
        cloudMaterial.SetFloat("_ErosionGain", erosionGain);
        cloudMaterial.SetFloat("_Coverage", coverage);
        cloudMaterial.SetFloat("_ErosionAmount", erosionAmount);
        cloudMaterial.SetFloat("_Absorption", absorption);
        cloudMaterial.SetFloat("_Density", density);
        cloudMaterial.SetFloat("_Powder", powder);
        cloudMaterial.SetVector("_Scattering", scattering);        
        
        cloudMaterial.SetFloat("_LightStepSize", lightStepSize);
        cloudMaterial.SetFloat("_Scale", scale);
        cloudMaterial.SetFloat("_ErosionScale", erosionScale);
        cloudMaterial.SetFloat("_WeatherScale", weatherScale);
        cloudMaterial.SetVector("_Stratus", stratusGradients);
        cloudMaterial.SetVector("_StratCu", stratocumulusGradients);
        cloudMaterial.SetVector("_Cumulus", cumulusGradients);

        cloudMaterial.SetVector("_WindDirection", Vector3.Normalize(windDirection));        
        cloudMaterial.SetFloat("_WindSpeed", windSpeed); //animation speed
        cloudMaterial.SetFloat("_WindSkewFactor", windSkewFactor);

        cloudMaterial.SetInt("_CloudLayer", cloudLayer);
    }

    #region Post-processing Effects
    //stores normalized rays from camera frustum in a 4x4 matrix
    //http://flafla2.github.io/2016/10/01/raymarching.html
    private Matrix4x4 GetFrustumCorners(Camera cam)
    {
        float camFov = cam.fieldOfView;
        float camAspect = cam.aspect;

        Matrix4x4 frustumCorners = Matrix4x4.identity;

        float fovWHalf = camFov * 0.5f;

        float tan_fov = Mathf.Tan(fovWHalf * Mathf.Deg2Rad);

        Vector3 toRight = Vector3.right * tan_fov * camAspect;
        Vector3 toTop = Vector3.up * tan_fov;

        Vector3 topLeft = (-Vector3.forward - toRight + toTop);
        Vector3 topRight = (-Vector3.forward + toRight + toTop);
        Vector3 bottomRight = (-Vector3.forward + toRight - toTop);
        Vector3 bottomLeft = (-Vector3.forward - toRight - toTop);

        frustumCorners.SetRow(0, topLeft);
        frustumCorners.SetRow(1, topRight);
        frustumCorners.SetRow(2, bottomRight);
        frustumCorners.SetRow(3, bottomLeft);

        return frustumCorners;
    }

    //render image effect as a quad on the screen
    [ImageEffectOpaque]
    void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        cloudMaterial.SetMatrix("_FrustumCornersES", GetFrustumCorners(Camera.main));
        cloudMaterial.SetMatrix("_CameraInvViewMatrix", Camera.main.cameraToWorldMatrix);
        cloudMaterial.SetVector("_CameraWS", Camera.main.transform.position);

        GraphicsBlit(src, dst, cloudMaterial, 0);
    }

    //http://flafla2.github.io/2016/10/01/raymarching.html
    static void GraphicsBlit(RenderTexture source, RenderTexture dest, Material mat, int passNr)
    {
        RenderTexture.active = dest;

        mat.SetTexture("_MainTex", source);

        GL.PushMatrix();
        GL.LoadOrtho(); // Note: z value of vertices don't make a difference because we are using ortho projection

        mat.SetPass(passNr);

        GL.Begin(GL.QUADS);

        // Here, GL.MultitexCoord2(0, x, y) assigns the value (x, y) to the TEXCOORD0 slot in the shader.
        // GL.Vertex3(x,y,z) queues up a vertex at position (x, y, z) to be drawn.  Note that we are storing
        // our own custom frustum information in the z coordinate.
        GL.MultiTexCoord2(0, 0.0f, 0.0f);
        GL.Vertex3(0.0f, 0.0f, 3.0f); // BL

        GL.MultiTexCoord2(0, 1.0f, 0.0f);
        GL.Vertex3(1.0f, 0.0f, 2.0f); // BR

        GL.MultiTexCoord2(0, 1.0f, 1.0f);
        GL.Vertex3(1.0f, 1.0f, 1.0f); // TR

        GL.MultiTexCoord2(0, 0.0f, 1.0f);
        GL.Vertex3(0.0f, 1.0f, 0.0f); // TL

        GL.End();
        GL.PopMatrix();
    }
    #endregion
}