﻿using UnityEngine;

[ExecuteInEditMode]
public class ProjectionScreen : MonoBehaviour {

	void Start () {

        gameObject.layer = 9;  //background
      
        transform.localScale = new Vector3(1.6f, 1f, 1f);
        transform.localPosition = Vector3.forward;
        transform.Rotate(new Vector3(-90f, 0f, 0f));
        
        //gameObject.hideFlags = HideFlags.HideAndDontSave;
    }
		
}
