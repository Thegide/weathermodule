﻿Shader "Skybox/Procedural.MaelstromStudios" {
	Properties{
		_SunSize("Sun Size", Range(0,1)) = 0.04
		_AtmosphericDensity("Atmospheric Density", Range(0,5)) = 1.0

		_MieG("Mie g", Range(-0.75,-0.999)) = -0.99
	}
		
	SubShader {
		Tags { "Queue" = "Background" "RenderType" = "Background" "PreviewType" = "Skybox" }
		Cull Off ZWrite Off

		Pass {

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc" //helper functions and macros


			//Constants
			const half kScaleHeight = 0.25;		//H0



			uniform float fKmESun;					//Km * ESun
			uniform float fKr4PI;					//Kr * 4 * PI	 Rayleigh constant
			uniform float fKm4PI;					//Km * 4 * PI    Mie constant

			//Variables
			uniform half _AtmosphericDensity;
			uniform float _SunSize;

			uniform half _MieG;
			

			//scatter ratio = 1 / pow(wavelength, 4);


			uniform float3 v3CameraPos;             // = 0 at sea level
			
			

			// Calculates the Rayleigh phase function (Hoffman and Preetham)
			half getRayleighPhase(half3 light, half3 ray)
			{
				half camCos = dot(light, ray);
				half camCos2 = camCos * camCos;
				return 0.059683 + 0.059683 * camCos2;  // 1/(16 * Pi)
			}

			//// Calculates the Rayleigh phase function (Henyey-Greenstein)
			//half getRayleighPhase(half3 light, half3 ray)
			//{
			//	half camCos = dot(light, ray);
			//	half camCos2 = camCos * camCos;
			//	return 0.75 + 0.75 * camCos2;
			//}

			// Calculates the Mie phase function where g is -0.999 to -0.75
			// values of g closer to 1 give more scatter (larger halo)
			half getMiePhase(half3 light, half3 ray) 
			{
				half MieG2 = _MieG * _MieG;				
				half camCos = dot(light, ray);
				half camCos2 = camCos * camCos;

				half first = 1.5  * (1 - MieG2) / (2 + MieG2);
				half second = 1 + camCos2 / pow((1 + MieG2 - 2 * MieG2 * camCos), 1.5);

				return first * second;
			}

			// Outscattering function

			half getOutScattering() {

			}


			//structs

			struct appdata_t 
			{
				float4 vertex : POSITION;				
				float2 uv: TEXCOORD0;
			};

			struct v2f 
			{
				float4 pos : SV_POSITION;
				float2 uv: TEXCOORD0;
				float3 vertex : TEXCOORD1;
				half3 sunColor : TEXCOORD2;
			};


			//vertex program
			v2f vert(appdata_t v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);  //maps vertices to view space
				o.uv = v.uv;
				
				
				
				// Get the ray from the camera to the vertex and its length (which is the far point of the ray passing through the atmosphere)
				float3 eyeRay = normalize(mul((float3x3)unity_ObjectToWorld, v.vertex.xyz));
				
				
				
				
				
				
				// Sun disk
				o.vertex = -v.vertex;
				
				
				return o;




			}


			//fragment program
			half4 frag(v2f i) : COLOR
			{
				fixed4 skyColor = getRayleighPhase(i.pos, i.pos);


				//fixed4 texColor = tex2D(_NightSkyTex, i.uv);

				//fixed4 blend = lerp(texColor, fixed4(_SkyColor, 1), _SkyAlpha);


				return skyColor;

			}

			ENDCG
		}
	}

}
