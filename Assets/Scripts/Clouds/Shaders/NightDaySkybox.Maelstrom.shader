﻿Shader "Skybox/NightDay.MaelstromStudios" {
	Properties {
		_SkyColor("Sky Color", Color) = (0.37, 0.52, 0.73, 0)	
		_SkyAlpha("Sky Transparency", Range(0,1)) = 1.0
		_NightSkyTex("Night Sky Albedo", 2D) = "white" {}
	}
		
	SubShader {
		Tags { "Queue" = "Background" "RenderType" = "Background" "PreviewType" = "Skybox" }
		Cull Off ZWrite Off

		Pass {

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc" //helper functions and macros


			//Variables


			struct Input {
				float2 uv_MainTex;
			};

			struct appdata_t //unity-provided data
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f //vertex to fragment
			{
				float4 pos : SV_POSITION;
				float2 uv: TEXCOORD0;
			};

			half3 _SkyColor;
			float _SkyAlpha;
			sampler2D _NightSkyTex;

			//vertex program
			v2f vert(appdata_t v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);  //maps vertices to view space
				o.uv = v.uv;
				return o;
			}


			//fragment program
			fixed4 frag(v2f i) : COLOR
			{
				fixed4 texColor = tex2D(_NightSkyTex, i.uv);
				
				fixed4 blend = lerp(texColor, fixed4(_SkyColor, 1), _SkyAlpha);
				

				return blend;

				//return half4(_SkyColor1, 1.0);
			}

			ENDCG
		}
	}

}
