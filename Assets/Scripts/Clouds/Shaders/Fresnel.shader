﻿Shader "Maelstrom/Fresnel" {
	Properties {
		_MainTex("Main Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		_SpecColor ("Specular Color", Color) = (1,1,1,1)
		_SpecShininess ("Specular Shininess", Range(1.0, 100.0)) = 2.0
		_FresnelPower ("Fresnel Power", Range(0.0, 3.0)) = 1.4
		_FresnelScale ("Fresnel Scale", Range(0.0, 1.0)) = 1.0
		_FresnelColor ("Fresnel Color", Color) = (1,1,1,1)

	}
	SubShader {

			Pass {
				Tags { "LightMode" = "ForwardBase" }
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc" //helper functions and macros

				struct appdata_t
				{
					float4 vertex : POSITION;
					float3 normal : NORMAL;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float4 pos : SV_POSITION;
					float3 normal : NORMAL;
					float2 uv: TEXCOORD0;					
					float4 posWorld : TEXCOORD1;
				};

				float4 _LightColor0;

				float4 _Color;
				sampler2D _MainTex;
				float4 _MainTex_ST;

				float4 _SpecColor;
				float _SpecShininess;

				float _FresnelPower;
				float _FresnelScale;
				float4 _FresnelColor;


				v2f vert(appdata_t v)
				{
					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.posWorld = mul(unity_ObjectToWorld, v.vertex);
					o.normal = mul(float4(v.normal, 0.0), unity_ObjectToWorld).xyz;
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					return o;
				}

				fixed4 frag(v2f i) : COLOR
				{
					float4 texColor = tex2D(_MainTex, i.uv);

					float3 normalDirection = normalize(i.normal);
					float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
					float3 viewDirection = normalize(_WorldSpaceCameraPos - i.posWorld.xyz);
					float3 diffuse = _LightColor0.rgb * _Color.rgb * max(0.0, dot(normalDirection, lightDirection));

					float3 specular;
					if (dot(normalDirection, lightDirection) < 0.0)
					{
						specular = float3 (0.0, 0.0, 0.0);
					}
					else
					{
						//multiply light color with specular color
						specular = _LightColor0.rgb * _SpecColor.rgb * pow(max(0.0, dot(reflect(-lightDirection, normalDirection), viewDirection)), _SpecShininess);
					}

					float3 j = i.posWorld - _WorldSpaceCameraPos.xyz;
					float refl = _FresnelScale * pow(1.0 + dot(normalize(j), normalDirection), _FresnelScale);

					float3 diffuseSpecular = diffuse + specular;

					float4 finalColor = float4(diffuseSpecular, 1) * texColor;

					return lerp(finalColor, _FresnelColor, refl);
				}
				ENDCG
			}
	}
	FallBack "Diffuse"
}
