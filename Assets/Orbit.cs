﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Orbit : MonoBehaviour {

    public TimeOfDay timeOfDay;
    public Transform orbiter;
    [Range(0,359)]public float moonPhase;
    public float radius = 10f;
	
	void LateUpdate ()
    {
        if (timeOfDay)
        {
            moonPhase = (timeOfDay.days * 12.5f) % 360;
        }        

        float x = Mathf.Sin(Mathf.Deg2Rad * moonPhase);
        float z = Mathf.Cos(Mathf.Deg2Rad * moonPhase);

        orbiter.localPosition = new Vector3(radius * x, 0, radius * z);
        orbiter.LookAt(transform);
    }
}
