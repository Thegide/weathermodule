﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Framerate : MonoBehaviour {

    private Text frameText;
    private Text fpsText;

	void Awake () {
        Text[] fields = GetComponentsInChildren<Text>();
        frameText = fields[0];
        fpsText = fields[1];

    }
	
	// Update is called once per frame
	void LateUpdate () {
        frameText.text = "Frame: " + Time.frameCount.ToString();
        fpsText.text = (int)(1 / Time.deltaTime) + " FPS";
	}
}
