﻿Shader "Volume/Fog" {

	Properties{
		_LightColor("Ambient Light Color", Color) = (1,1,1,1)
		_LightDir("Light Vector", Vector) = (0,0,0,0)

		_Beers("Beers Law Multiplier", Float) = 2
		_Phase("Henyey-Greenstein Multiplier", Float) = 1
		_Scatter("In-Scattering Probability Multiplier", Float) = 1

		_MainTex("Base (RGB)", 2D) = "white" {}
		_Volume("Volume", 3D) = "" {}		
	}

	Category{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off Lighting Off ZWrite Off
		ZTest Always

	SubShader{

		Pass{
			//Tags{"Lightmode" = "Deferred"}
			CGPROGRAM
			#pragma target 3.0
			
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "Lighting.cginc"

			inline float CalcVolumeFogIntensity(float3 sphereCenter, float sphereRadius, float3 cameraPosition, float3 viewDirection, float backDepth, float maxDistance, float density)
			{
				float3 local = cameraPosition - sphereCenter;
				float  fA = dot(viewDirection, viewDirection);
				float  fB = 2 * dot(viewDirection, local);
				float  fC = dot(local, local) - sphereRadius * sphereRadius;
				float  fD = fB * fB - 4 * fA * fC;
				
				if (fD < 0.0f)
					return 0;

				float recpTwoA = 0.5 / fA;

				float dist;

				if (fD == 0.0f)
				{
					dist = backDepth;
				}	
				else
				{
					float DSqrt = sqrt(fD);
					dist = (-fB - DSqrt) * recpTwoA;
				}

				dist = min(dist, maxDistance);
				backDepth = min(backDepth, maxDistance);

				float sample = dist;
				float fog = 0;
				float step_distance = (backDepth - dist) / 10;
				for (int seg = 0; seg < 10; seg++)
				{
					float3 position = cameraPosition + viewDirection * sample;
					fog += 1 - saturate(length(sphereCenter - position) / sphereRadius);
					sample += step_distance;
				}
				fog /= 10;
				fog = saturate(fog * density);
				return fog;
			}

			fixed4 _FogColor;
			sampler2D _MainTex;
			sampler3D _Volume;
			uniform float4 FogParam;


			struct appdata
			{
				float4 vertex : POSITION;
				float3 uv : TEXCOORD0;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				float3 view : TEXCOORD0;
				float4 projPos : TEXCOORD1;
			};


			//vertex program
			v2f vert(appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);  //maps vertices to view space
				o.view = v.uv;
				o.projPos = mul((float4x4)unity_ObjectToWorld, v.vertex.xyz); //get world position
				return o;
			}

			half4 frag(v2f i) : COLOR
			{
				half4 color = half4(1,1,1,1);
				float4 noiseSample = tex3Dlod(_Volume, i.pos);
				float noise = saturate((noiseSample.r + noiseSample.g + noiseSample.b + noiseSample.a) / 4.0);

				float depth = noise;
				float backDist = length(i.view);
				float3 viewDir = normalize(i.view);
				float fog = CalcVolumeFogIntensity(FogParam.xyz, FogParam.w, _WorldSpaceCameraPos, viewDir, backDist, depth,_FogColor.a);

				color.rgb = _FogColor.rgb;
				color.a = fog;
				return color;
			}
			ENDCG

			}
		}
	}
	Fallback Off
}