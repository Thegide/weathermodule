﻿/*Volumetric cloud shader (c) Maelstrom Studios 2018
References:
Hogfeldt, R. 2016
Hillaire, S. 2016
Palenik, J. 2016
*/

Shader "Volume/Clouds3" {
	Properties{
		_WeatherTex("Weather Texture", 2D) = "" {}
		_CloudTex("Cloud Shape Texture", 3D) = "" {}
		_CloudDetail("Cloud Detail Texture", 3D) = "" {}

		_Color("Ambient Light Color", Color) = (1, 1, 1, 1)

		_EarthRadius("Earth Radius", Float) = 6000					//metres
		_CloudStartHeight("Atmosphere Start Height", Float) = 200
		_CloudEndHeight("Atmosphere End Height", Float) = 400
		_CloudLayer("Cloud Layer", Int) = 0

		_Steps("Raymarch Steps", Range(1,256)) = 64
		_Gain("Noise Gain", Range(0.1, 10.0)) = 0.7
		_Scale("Noise Scale", Range(1,512)) = 512
		_WeatherScale("Weather Scale", Range(0.1,10)) = 1

		[Toggle]_Shaping("Shaping Enabled", Range(0,1)) = 1
		[Toggle]_Details("Details Enabled", Range(0,1)) = 1
		[Toggle]_Lighting("Lighting Enabled", Range(0,1)) = 1
		_Absorption("Absorption coefficient", Float) = 0.04343
		_Powder("Powder", Float) = 1.0
		_Scattering("Scattering coefficient", Vector) = (0.5, 0.7, 1.0, 1)
		_LightStepSize("Light Step Size", Float) = 1.0
		
		//_Blend("Perlin-Worley Blend", Range(0.0, 1.0)) = 0.5

		_Speed("Animation Speed", Float) = 50
		
	}
		
	SubShader {
		Tags{"LightMode" = "Deferred"}
		Cull Off ZWrite Off ZTest Always

		Pass {

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			//#pragma enable_d3d11_debug_symbols  //renderdoc debugging - remove for optimization

			#include "UnityCG.cginc" //helper functions and macros
			#include "Lighting.cginc"
			#include "VolumeMath.cginc"
			
			//volume projection
			uniform float4x4 _FrustumCornersES;
			uniform float4x4 _CameraInvViewMatrix;
			uniform float4 _MainTex_TexelSize;

			//textures
			uniform sampler2D _MainTex;					//post-processing input
			uniform sampler2D _CameraDepthTexture;		//sky culling
			uniform sampler2D _WeatherTex;
			uniform sampler3D _CloudTex;
			uniform sampler3D _CloudDetail;

			float4 _Color;
			float _Steps;
			float _Lighting;
			float _Shaping;			
			float _Details;
			float _Absorption;
			float _Density;
			float _Powder;
			float4 _Scattering;
			float _LightStepSize;
			
			float _EarthRadius;
			float _CloudStartHeight;
			float _CloudEndHeight;
			int _CloudLayer;
						
			float _Scale;
			float _WeatherScale;
			float _Coverage;
			float _ShapeGain;
			float _ErosionGain;			
			float _Erosion;

			float4 _Stratus;
			float4 _StratCu;
			float4 _Cumulus;
			
			float _Speed;

			#define EYEPOS _WorldSpaceCameraPos

			#define kInnerRadius (_EarthRadius + _CloudStartHeight)
			#define kOuterRadius (_EarthRadius + _CloudEndHeight)
			#define kInnerRadius2 (kInnerRadius * kInnerRadius)
			#define kOuterRadius2 (kOuterRadius * kOuterRadius)
			#define kEarthCenter float3(0.0, -_EarthRadius, 0.0)

			#define BELOW_CLOUDS 0
			#define IN_CLOUDS 1
			#define ABOVE_CLOUDS 2
			#define MAX_RAYMARCH_DISTANCE 2000

			#define STEPS _Steps			
			#define LIGHTSTEPS 6
			#define WEATHERSCALE 0.001 / _WeatherScale
			#define SCALE 0.001 / _Scale

			// random vectors on the unit sphere
			const float3 RANDOM_VECTORS[6] =
			{
				float3(0.38051305, 0.92453449, -0.02111345),
				float3(-0.50625799, -0.03590792, -0.86163418),
				float3(-0.32509218, -0.94557439, 0.01428793),
				float3(0.09026238, -0.27376545, 0.95755165),
				float3(0.28128598, 0.42443639, -0.86065785),
				float3(-0.16852403, 0.14748697, 0.97460106)
			};

			//structs
			struct appdata 
			{
				float4 vertex : POSITION;
				float3 uv : TEXCOORD0;
			};

			struct v2f 
			{
				float4 pos : SV_POSITION;		//clip space
				float3 uv : TEXCOORD0;
				float3 wPos : TEXCOORD1;		//world position
				float3 ray : TEXCOORD2;
			};
			
			//function declarations			
			float4 raymarch(float3 startPos, float3 endPos, float3 lightRay);
			float getCloudDensity(float3 pos, float3 weatherData);
			float3 getCoverage(float2 pos);

			//vertex program
			v2f vert(appdata v)
			{
				v2f o;

				half index = v.vertex.z;
				v.vertex.z = 0.1;

				o.pos = UnityObjectToClipPos(v.vertex);  //maps vertices to view space
				o.uv = v.uv;

				#if UNITY_UV_STARTS_AT_TOP									//prevent flipped textures on some DX machines
				if (_MainTex_TexelSize.y < 0)
					o.uv.y = 1 - o.uv.y;
				#endif
				
				o.wPos = mul((float3x3)unity_ObjectToWorld, v.vertex.xyz); //get world position				
				o.ray = _FrustumCornersES[(int)index].xyz;
				o.ray = normalize(mul(_CameraInvViewMatrix, o.ray));
				return o;
			}

			//fragment program
			fixed4 frag(v2f i) : SV_Target
			{		
				float3 lightRay = normalize(_WorldSpaceLightPos0 - EYEPOS);		//direction of light

				float3 entry = 0;
				float3 exit = 0;

				//entry = raySphereIntersect(EYEPOS, i.ray, kEarthCenter, kInnerRadius2);
				//exit = raySphereIntersect(EYEPOS, i.ray, kEarthCenter, kOuterRadius2);

				float2 inner_shell_hit;
				float2 outer_shell_hit;
				uint num_inner_hits;
				uint num_outer_hits;
				
				if (_CloudLayer == BELOW_CLOUDS)
				{
					raySphereIntersect(EYEPOS, i.ray, kEarthCenter, kInnerRadius2, inner_shell_hit);
					entry = EYEPOS + i.ray * inner_shell_hit.x;
					raySphereIntersect(EYEPOS, i.ray, kEarthCenter, kOuterRadius2, outer_shell_hit);
					exit = EYEPOS + i.ray * outer_shell_hit.x;
				}
				else if (_CloudLayer == IN_CLOUDS)
				{
					entry = EYEPOS;
					num_inner_hits = raySphereIntersect(EYEPOS, i.ray, kEarthCenter, kInnerRadius2, inner_shell_hit);
					num_outer_hits = raySphereIntersect(EYEPOS, i.ray, kEarthCenter, kOuterRadius2, outer_shell_hit);

					if (num_inner_hits == 0)
					{
						exit = EYEPOS + i.ray * outer_shell_hit.x;
					}
					else  
					{
						exit = EYEPOS + i.ray * min(inner_shell_hit.x, outer_shell_hit.x);
					}

				}
				else if (_CloudLayer == ABOVE_CLOUDS)
				{
					num_inner_hits = raySphereIntersect(EYEPOS, i.ray, kEarthCenter, kInnerRadius2, inner_shell_hit);
					num_outer_hits = raySphereIntersect(EYEPOS, i.ray, kEarthCenter, kOuterRadius2, outer_shell_hit);

					if (num_outer_hits > 0)
					{
						entry = EYEPOS + i.ray * outer_shell_hit.x;
						if (num_inner_hits > 0) 
						{
							exit = EYEPOS + i.ray * inner_shell_hit.x;
						}
						else
						{
							exit = EYEPOS + i.ray * outer_shell_hit.y;
						}
					}
				}
				else return 0;

				//do not render clouds below the horizon
				if (entry.y < 0.0)
				{
					return tex2D(_MainTex, i.uv);
				}

				//apply effect to skybox only by occlusion
				float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv);				
				if (depth < 0.00001) {

					fixed4 cloudColor = raymarch(entry, exit, lightRay);				//ray march from the vertex back to the camera
					return lerp(tex2D(_MainTex, i.uv), cloudColor, cloudColor.a);		//blend with source render texture
				}				
				else { return tex2D(_MainTex, i.uv); }
			}

			float heightFraction(float3 pos)
			{
				float height = distance(pos, kEarthCenter);
				return saturate(remap(height, kInnerRadius, kOuterRadius, 0.0, 1.0));
			}

			float densityGradient(float3 relativeHeight, float cloudType)
			{
				relativeHeight = saturate(relativeHeight);

				/* Defaults provided by Guerilla Games
				* float cumulus = max(0.0, remap(relativeHeight, 0.01, 0.3, 0.0, 1.0) * remap(relativeHeight, 0.6, 0.95, 1.0, 0.0));
				* float stratocumulus = max(0.0, remap(relativeHeight, 0.0, 0.25, 0.0, 1.0) * remap(relativeHeight, 0.3, 0.65, 1.0, 0.0));
				* float stratus = max(0.0, remap(relativeHeight, 0, 0.1, 0.0, 1.0) * remap(relativeHeight, 0.2, 0.3, 1.0, 0.0));
				*/

				float cumulus = max(0.0, remap(relativeHeight, _Cumulus.x, _Cumulus.y, 0.0, 1.0) * remap(relativeHeight, _Cumulus.z, _Cumulus.w, 1.0, 0.0));
				float stratocumulus = max(0.0, remap(relativeHeight, _StratCu.x, _StratCu.y, 0.0, 1.0) * remap(relativeHeight, _StratCu.z, _StratCu.w, 1.0, 0.0));
				float stratus = max(0.0, remap(relativeHeight, _Stratus.x, _Stratus.y, 0.0, 1.0) * remap(relativeHeight, _Stratus.z, _Stratus.w, 1.0, 0.0));

				float a = lerp(stratus, stratocumulus,  saturate(cloudType * 2.0));

				float b = lerp(stratocumulus, stratus, saturate((cloudType - 0.5) * 2.0));
				return lerp(a, b, cloudType);
			}

			float3 getCoverage(float2 pos)
			{				
				return tex2Dlod(_WeatherTex, float4(pos * WEATHERSCALE, 1.0, 1.0)).rgb;
			}

			float getCloudDensity(float3 pos, float3 weatherData)
			{
				//build base cloud shape
				float4 low_freq_noise = tex3Dlod(_CloudTex, float4(pos * SCALE, 1)) *_ShapeGain;
				float low_freq_detail = (low_freq_noise.g * 0.625) + (low_freq_noise.b * 0.25) + (low_freq_noise.a * 0.125);
				
				float baseCloud = (remap(low_freq_noise.r, -(1.0 - low_freq_detail), 1.0, 0.0, 1.0));

				//use weather map to define coverage
				float coverage = weatherData.r * _Coverage;
				float relativeHeight = heightFraction(pos);	
				float cloudType = weatherData.g;
						
				baseCloud *= densityGradient(relativeHeight, cloudType);
					
				//float baseCloudCoverage = baseCloud * coverage; 
				float baseCloudCoverage = remap(baseCloud, coverage, 1.0, 0.0, 1.0);
				baseCloudCoverage *= coverage;							

				if (!_Details) { return saturate(baseCloudCoverage); }

				//erode details
				float4 hi_freq_noise = tex3Dlod(_CloudDetail, float4(pos * 20 * SCALE, 1)) * _ErosionGain;
				float hi_freq_detail = (hi_freq_noise.r * 0.625) + (hi_freq_noise.g * 0.25) + (hi_freq_noise.b * 0.125);
				float noiseModifier = lerp(hi_freq_detail, 1.0 - hi_freq_detail, saturate(relativeHeight * 2.0)); 

				float finalCloud = saturate(remap(baseCloudCoverage, noiseModifier * _Erosion, 1.0, 0.0, 1.0));

				return finalCloud * _Density;
			}
			
			float3 evaluateLighting(float3 startPos, float3 weatherData, float cosTheta)
			{
				float3 pos = startPos;
				float3 dir = _WorldSpaceLightPos0;
				float stepSize = _LightStepSize * 20;

				float coneRadius = 5.0;
				float3 conePos;

				//light accumulation variables
				float transmittance = 1;
				float density = 0;

				for (int i = 0; i < LIGHTSTEPS; i++)
				{
					conePos = pos + coneRadius * RANDOM_VECTORS[i] * (1 + 1);

					if (_Shaping) 
					{
						density += getCloudDensity(conePos, weatherData);
					}
					else 
					{
						density = getCoverage(conePos.xz).r;
						break;
					}
					pos += dir * stepSize;
				}

				density = saturate(density);
				transmittance = exp(-density);

				return _Color.rgb * transmittance * hgPhase(0.6, cosTheta) * _Scattering.x;
			}

			float4 raymarch(float3 cloudStart, float3 cloudEnd, float3 light)
			{				
				//initialize raymarch position and march vectors
				float3 pos = cloudStart;
				float3 rayDelta = cloudEnd - cloudStart;
				float3 ray = normalize(rayDelta);	

				float stepSize = length(rayDelta / STEPS);

				//light calculation coefficients
				const float abs_mult = -_Absorption * stepSize;

				//for phase function
				float cosTheta = dot(light, ray);

				//color accumulation variables
				float transmittance = 1;
				float3 color = 0;			
				float density = 0;

				for (int i = 0; i < STEPS; i++)
				{
					float3 weatherData = getCoverage(pos.xz);
					
					//evaluate density at current position				
					if (_Shaping)
					{
						density += getCloudDensity(pos, weatherData);  // * 0.005  makes for softer clouds
					}
					else 
					{
						density = weatherData.r;
					}
					
					//evaluate trasmittance
					float T = exp(abs_mult * density);  //Beer-Lambert law
					//T = beerPowder(density, stepSize, _Absorption, _Powder);

					//evaluate lighting via single scattering for all samples with density greater than zero	
					transmittance *= T;

					if (density > 0.0 && _Lighting)
					{
						color = color * (1.0 - transmittance) + evaluateLighting(pos, weatherData, cosTheta) * transmittance;
					}

					if (transmittance < 1e-6)
						break;				

					if (!_Shaping)
						break;

					//increment the step
					pos += ray * stepSize;
				}			

				float opacity = 1.0 - transmittance;

				//final cloud color		
				if (_Lighting) {
					return float4(color * opacity, opacity);
				}
				else {
					return float4(0.0, 0.0, 0.0, opacity);						//transmittance only
				}
			}
		

				
			ENDCG
		}
	}
	Fallback Off
}
