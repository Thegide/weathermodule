﻿//Atmosphere "dome" projection for inside atmosphere only
float3 raySphereIntersect(float3 rayOrigin, float3 ray, float3 sphereCenter, float radius2)
{
	//Ray-sphere intersection equation
	float3 startPosition = rayOrigin - sphereCenter;

	float a = dot(ray, ray);													//A = d DOT d
	float b = dot(ray, startPosition) * 2;										//B = (o-c) DOT d * 2
	float c = dot(startPosition, startPosition) - radius2;						//C = (o-c) DOT (o-c) - rad^2
	float discr = b * b - 4.0f * a * c; 										//disc = (b*b) - 4*a*c
		
	float t = max(0, 0.5f * (-b + sqrt(discr)) / a);

	return rayOrigin + ray * t;
}

//Atmosphere "dome" projection for any position
uint raySphereIntersect(float3 rayOrigin, float3 ray, float3 sphereCenter, float radius2, out float2 hit)
{
	//Ray-sphere intersection equation
	float3 startPosition = rayOrigin - sphereCenter;

	float a = dot(ray, ray);													//A = d DOT d
	float b = dot(ray, startPosition) * 2;										//B = (o-c) DOT d * 2
	float c = dot(startPosition, startPosition) - radius2;						//C = (o-c) DOT (o-c) - rad^2
	float discr = b * b - 4.0f * a * c; 										//disc = (b*b) - 4*a*c

	//no intersection
	if (discr < 0.0f)
	{
		hit.x = 0.0f;
		hit.y = 0.0f;
		return 0u;
	}

	//intersection as tangent
	else if (abs(discr) - 0.0001f <= 0.0f)
	{
		float t = -0.5f * b / a;
		hit.x = t;
		hit.y = t;
		return 1u;
	}

	//normal intersection
	else
	{
		float q = (b > 0.0f) ? -0.5f * (b + sqrt(discr)) : -0.5f * (b - sqrt(discr));
		float h1 = q / a;
		float h2 = c / q;

		hit.x = min(h1, h2);
		hit.y = max(h1, h2);

		if (hit.x < 0.0f)
		{
			hit.x = hit.y;
			if (hit.x < 0.0f) 
			{
				return 0u;
			}		
			return 1u;
		}
		return 2u;
	}
}

half rayleighPhase(half cosTheta)
{
	half cosTheta2 = cosTheta * cosTheta;
	return 0.75 + 0.75 * cosTheta2;
}

half miePhase(half cosTheta)
{
	half MieG = -0.99;
	half MieG2 = MieG * MieG;
	half cosTheta2 = cosTheta * cosTheta;

	half first = 1.5  * (1 - MieG2) / (2 + MieG2);
	half second = 1 + cosTheta2 / pow((1 + MieG2 - 2 * MieG2 * cosTheta), 1.5);
	
	return first * second * 100;  //100 for scale since results are weaker than for Rayleigh
}

#define ONE_OVER_4PI 0.079577471545947
//Henyey-Greenstein Phase function - single in-scattering model
half hgPhase(half g, half cosTheta)
{
	half g2 = g * g;
	half cosTheta2 = cosTheta * cosTheta;

	half first = 1.0 - g2;
	half second = pow((1.0 + g2 - 2.0 * g * cosTheta), 1.5);
	return ONE_OVER_4PI * first / second; //40 constant for scale
}

float remap(float value, float original_min, float original_max, float new_min, float new_max)
{
	return new_min + (((value - original_min) / (original_max - original_min)) * (new_max - new_min));
}

float beerPowder(float density, float depth, float _Absorption, float _Powder)
{
	//e ^ (-t * d)
	//t = extinction coefficient
	//depth = optical depth, calculated from step length
	float beer = exp(-_Absorption * density * depth);			//Beer-Lambert's law - transmittance of incident light
	float powder = 1 - exp(-_Powder * density * depth);
	return beer * powder;                                       //non directional.  see Palenik p28 for how to implement directionality
}


