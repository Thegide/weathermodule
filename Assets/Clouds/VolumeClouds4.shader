﻿/*Volumetric cloud shader (c) Maelstrom Studios 2018
References:
Hogfeldt, R. 2016
Hillaire, S. 2016
Palenik, J. 2016
*/

Shader "Volume/Clouds4" {
	Properties{
		_WeatherTex("Weather Texture", 2D) = "" {}
		_CloudTex("Cloud Shape Texture", 3D) = "" {}
		_CloudDetail("Cloud Detail Texture", 3D) = "" {}
		_CurlNoise("Curl Texture", 2D) = "" {}

		_Color("Ambient Light Color", Color) = (1, 1, 1, 1)

		_EarthRadius("Earth Radius", Float) = 6000					//metres
		_CloudStartHeight("Atmosphere Start Height", Float) = 200
		_CloudEndHeight("Atmosphere End Height", Float) = 400
		_CloudLayer("Cloud Layer", Int) = 0

		_Steps("Raymarch Steps", Range(1,256)) = 64
		_Gain("Noise Gain", Range(0.1, 10.0)) = 0.7
		_Scale("Noise Scale", Range(1,512)) = 512
		_WeatherScale("Weather Scale", Range(0.1,10)) = 1

		[Toggle]_Shaping("Shaping Enabled", Range(0,1)) = 1
		[Toggle]_Details("Details Enabled", Range(0,1)) = 1
		[Toggle]_Lighting("Lighting Enabled", Range(0,1)) = 1
		[Toggle]_ApplyFog("Fog", Range(0,1)) = 0
		_Absorption("Absorption coefficient", Float) = 0.04343
		_Powder("Powder", Float) = 1.0
		_Scattering("Scattering coefficient", Vector) = (0.5, 0.7, 1.0, 1)
		_LightStepSize("Light Step Size", Float) = 1.0
		
		//_Blend("Perlin-Worley Blend", Range(0.0, 1.0)) = 0.5

		_Speed("Animation Speed", Float) = 50
		
	}
		
	SubShader {
		Tags{"LightMode" = "Deferred"}
		Cull Off ZWrite Off ZTest Always

		Pass {

			CGPROGRAM			

			#pragma vertex vert
			#pragma fragment frag
			//#pragma enable_d3d11_debug_symbols  //renderdoc debugging - remove for optimization

			#include "UnityCG.cginc" //helper functions and macros
			#include "Lighting.cginc"
			#include "VolumeMath.cginc"
			
			//volume projection
			uniform float4x4 _FrustumCornersES;
			uniform float4x4 _CameraInvViewMatrix;
			uniform float4 _MainTex_TexelSize;

			//textures
			uniform sampler2D _MainTex;					//post-processing input
			uniform sampler2D _CameraDepthTexture;		//sky culling
			uniform sampler2D _WeatherTex;
			uniform sampler3D _CloudTex;
			uniform sampler3D _CloudDetail;
			uniform sampler2D _CurlNoise;

			uint _Frame;
			int2 _ScreenSize;

			float4 _Color;
			float _Steps;
			float _Lighting;
			float _Shaping;			
			float _Details;
			int _Fog;

			float _Absorption;
			float _Density;
			float _Powder;
			float4 _Scattering;
			float _LightStepSize;
			
			float _EarthRadius;
			float _CloudStartHeight;
			float _CloudEndHeight;
			int _CloudLayer;
						
			float _Scale;
			float _WeatherScale;
			float _Coverage;
			float _ShapeGain;			
			float _ErosionAmount;
			float _ErosionGain;
			float _ErosionScale;

			float4 _Stratus;
			float4 _StratCu;
			float4 _Cumulus;
			
			float4 _WindDirection;			
			float _WindSpeed;
			float _WindSkewFactor;

			#define EYEPOS _WorldSpaceCameraPos

			#define kInnerRadius (_EarthRadius + _CloudStartHeight)
			#define kOuterRadius (_EarthRadius + _CloudEndHeight)
			#define kInnerRadius2 (kInnerRadius * kInnerRadius)
			#define kOuterRadius2 (kOuterRadius * kOuterRadius)
			#define kEarthCenter float3(0.0, -_EarthRadius, 0.0)

			#define BELOW_CLOUDS 0
			#define IN_CLOUDS 1
			#define ABOVE_CLOUDS 2
			#define MAX_RAYMARCH_DISTANCE 2000

			#define STEPS _Steps			
			#define LIGHTSTEPS 6
			#define WEATHERSCALE 0.0005 / _WeatherScale
			#define SCALE 0.001 / _Scale

			#define STRATUS_GRADIENT _Stratus
			#define STRATOCUMULUS_GRADIENT _StratCu
			#define CUMULUS_GRADIENT _Cumulus

			// The order in which we update the clouds pixels
			static int2 reproj_offset[16] = 
			{
				int2(2,1), int2(1,2), int2(2,0), int2(0,1),
				int2(2,3), int2(3,2), int2(3,1), int2(0,3),
				int2(1,0), int2(1,1), int2(3,3), int2(0,0),
				int2(2,2), int2(1,3), int2(3,0), int2(0,2)
			};

			// random vectors on the unit sphere
			static float3 random_vectors[6] =
			{
				float3(0.38051305, 0.92453449, -0.02111345),
				float3(-0.50625799, -0.03590792, -0.86163418),
				float3(-0.32509218, -0.94557439, 0.01428793),
				float3(0.09026238, -0.27376545, 0.95755165),
				float3(0.28128598, 0.42443639, -0.86065785),
				float3(-0.16852403, 0.14748697, 0.97460106)
			};

			//structs
			struct appdata 
			{
				float4 vertex : POSITION;
				float3 uv : TEXCOORD0;
			};

			struct v2f 
			{
				float4 pos : SV_POSITION;		//clip space
				float3 uv : TEXCOORD0;
				float3 wPos : TEXCOORD1;		//world position
				float3 ray : TEXCOORD2;
			};
			
			//function declarations			
			float4 raymarch(float3 startPos, float3 endPos, float3 lightRay);
			float getCloudDensity(float3 pos, float3 weatherData);
			float3 getCoverage(float2 pos);
			uint getCloudLayer(float3 ray, out float3 entry, out float3 exit);

			v2f vert(appdata v)
			{
				v2f o;

				half index = v.vertex.z;
				v.vertex.z = 0.1;

				o.pos = UnityObjectToClipPos(v.vertex);  //maps vertices to view space
				o.uv = v.uv;

				//prevent flipped textures on some DX machines				
				#if UNITY_UV_STARTS_AT_TOP									
				if (_MainTex_TexelSize.y < 0)
					o.uv.y = 1 - o.uv.y;
				#endif
				
				o.wPos = mul((float3x3)unity_ObjectToWorld, v.vertex.xyz); //get world position				
				o.ray = _FrustumCornersES[(int)index].xyz;
				o.ray = normalize(mul(_CameraInvViewMatrix, o.ray));
				return o;
			}

			float4 frag(v2f i) : SV_Target
			{
				//calculate pixels to be updated - 1/16th of the pixels raymarched/updated each frame
				//implementation based on stingray plugin
				int2 pixel_pos = (int2)i.pos;
				int2 pixel_id = pixel_pos - reproj_offset[_Frame % 16];
				int2 pixel_query = (pixel_id % int2(4, 4)) == int2(0, 0);
				bool update_pixel = (pixel_query.x && pixel_query.y);

				//update the pixel by raymarching
				if (update_pixel) {
					return float4(1, 0, 0, 1);
				}
				else
				{
					//do it by temporal reprojection
				}

				float3 lightRay = normalize(_WorldSpaceLightPos0 - EYEPOS);		//direction of light

				float3 entry = 0;
				float3 exit = 0;
				getCloudLayer(i.ray, entry, exit);			

				//get scene depth
				float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv);
				depth = 1 - depth;

				//cull cloud rendering below horizon and where occluded by objects				
				float4 sceneColor = tex2D(_MainTex, i.uv);
				if (entry.y < 0.0 || depth < 0.99999)
				{
					return sceneColor;
				}

				//raymarch clouds and blend with background sky
				float4 cloudColor = raymarch(entry, exit, lightRay);		
				sceneColor *= 1 + cloudColor.a;
				sceneColor = lerp(sceneColor, cloudColor, cloudColor.a);  
				
				//apply fog to horizon
				if (_Fog == 1 && depth > 0.99999) {
					//fog distance
					float fogAmount = 1.0 - exp(-distance(EYEPOS, entry) * 0.0001);
					float3 fogColor = float3(0.3, 0.4, 0.45) * length(_Color.rgb * 0.125) * 0.8f;
					float3 sunColor = normalize(_Color.rgb) * 4.0 * length(_Color.rgb * 0.125);
					fogColor = lerp(fogColor, sunColor, pow(saturate(dot(i.ray, _WorldSpaceLightPos0)), 0.2));
					return float4(clamp(lerp(sceneColor, fogColor, fogAmount), 0.0, 1000.0), saturate(sceneColor.a));
				}

				return sceneColor;
			}

			uint getCloudLayer(float3 ray, out float3 entry, out float3 exit)
			{
				float2 inner_shell_hit;
				float2 outer_shell_hit;

				uint num_inner_hits = raySphereIntersect(EYEPOS, ray, kEarthCenter, kInnerRadius2, inner_shell_hit);
				uint num_outer_hits = raySphereIntersect(EYEPOS, ray, kEarthCenter, kOuterRadius2, outer_shell_hit);

				if (_CloudLayer == BELOW_CLOUDS)
				{
					entry = EYEPOS + ray * inner_shell_hit.x;
					exit = EYEPOS + ray * outer_shell_hit.x;
					return 1;
				}
				else if (_CloudLayer == IN_CLOUDS)
				{
					entry = EYEPOS;

					if (num_inner_hits == 0)
					{
						exit = EYEPOS + ray * outer_shell_hit.x;
					}
					else
					{
						exit = EYEPOS + ray * min(inner_shell_hit.x, outer_shell_hit.x);
					}
					return 1;
				}
				else if (_CloudLayer == ABOVE_CLOUDS)
				{
					if (num_outer_hits > 0)
					{
						entry = EYEPOS + ray * outer_shell_hit.x;
						if (num_inner_hits > 0)
						{
							exit = EYEPOS + ray * min(inner_shell_hit.x, inner_shell_hit.y);  //working like shit
						}
						else
						{
							exit = EYEPOS + ray * outer_shell_hit.y;
						}
					}
					return 1;
				}
				return 0;
			}

			float3 getCoverage(float2 pos)
			{
				pos += _WindDirection.xz * _Time.y * _WindSpeed;
				return tex2Dlod(_WeatherTex, float4(pos * WEATHERSCALE, 1.0, 0.0)).rgb;
			}

			float heightFraction(float3 pos)
			{
				float height = distance(pos, kEarthCenter);
				return saturate(remap(height, kInnerRadius, kOuterRadius, 0.0, 1.0));
			}		

			float densityGradient(float3 relativeHeight, float cloudType)
			{
				relativeHeight = saturate(relativeHeight);

				//cloud type 0.0 = stratus, 0.5 = stratocumulus, 1.0 = cumulus				
				float stratus = 1.0f - saturate(cloudType * 2.0f);              //values 0.0 to 0.5, linear max at 0
				float stratocumulus = 1.0f - abs(cloudType - 0.5f) * 2.0f;      //values 0 to 1, parabolic max at 0.5
				float cumulus = saturate(cloudType - 0.5f) * 2.0f;				//values 0.5 to 1, linear max at 1

				float4 cloudGradient = STRATUS_GRADIENT * stratus + STRATOCUMULUS_GRADIENT * stratocumulus + CUMULUS_GRADIENT * cumulus;
				return smoothstep(cloudGradient.x, cloudGradient.y, relativeHeight) - smoothstep(cloudGradient.z, cloudGradient.w, relativeHeight);
			}
			
			/* DENSITY FUNCTION -----------------------------------------------------------------
			*  Controls all aspects of cloud shaping by providing density data to the ray marcher
			*  Algorithm by Andrew Schneider/Guerilla Games
			*/
			float getCloudDensity(float3 pos, float3 weatherData)
			{
				float relativeHeight = heightFraction(pos);

				//adjust position based on weather data (wind)				
				pos += _WindDirection.xyz * _Time.y * _WindSpeed;					//NUBIS slide 45
				pos += _WindDirection.xyz * relativeHeight * _WindSkewFactor;
				pos *= SCALE;

				//build base cloud shape
				float4 low_freq_noise = tex3Dlod(_CloudTex, float4(pos, 0)) *_ShapeGain;
				float low_freq_detail = (low_freq_noise.g * 0.625) + (low_freq_noise.b * 0.25) + (low_freq_noise.a * 0.125);
				
				float baseCloud = remap(low_freq_noise.r, -(1.0 - low_freq_detail), 1.0, 0.0, 1.0);   //NUBIS slide 36

				//use weather map to define coverage
				float coverage = weatherData.r * _Coverage;

				//adjust cloud density based on cloud type
				float cloudType = weatherData.g;						
				baseCloud *= densityGradient(relativeHeight, cloudType);
					
				float baseCloudCoverage = remap(baseCloud, 1.0 - coverage, 1.0, 0.0, 1.0);  //NUBIS slide 40
				baseCloudCoverage *= coverage;							

				if (!_Details) { return saturate(baseCloudCoverage); }

				//add in curl noise
				pos += tex2Dlod(_CurlNoise, float4(pos.xz, 1.0, 1.0)) * (1.0 - relativeHeight);

				//erode details
				float4 hi_freq_noise = tex3Dlod(_CloudDetail, float4(pos * _ErosionScale, 0)) * _ErosionGain;
				float hi_freq_detail = (hi_freq_noise.r * 0.625) + (hi_freq_noise.g * 0.25) + (hi_freq_noise.b * 0.125);
				float noiseModifier = lerp(hi_freq_detail, 1.0 - hi_freq_detail, saturate(relativeHeight * 2.0)); 

				float finalCloud = saturate(remap(baseCloudCoverage, noiseModifier * _ErosionAmount, 1.0, 0.0, 1.0));

				return finalCloud * _Density;
			}
			
			float beerLambert(float density, float abs_mult)
			{
				return exp(density * abs_mult);
			}

			float powder(float density, float cosTheta) 
			{
				float p = 1.0 - exp(-density * 2.0);
				return lerp(1.0, p, remap(-cosTheta, -1.0, 1.0, 0.0, 1.0));
			}

			float lightEnergy(float cosTheta, float density, float stepSize) 
			{
				return 2.0 * beerLambert(density, _Absorption * stepSize) * powder(density, cosTheta) * lerp (hgPhase(0.8, cosTheta), hgPhase(-0.5, cosTheta), 0.5);

			}

			float ambientLight(float relativeHeight)
			{
				return lerp(float3(0.5, 0.67, 0.82), float3(1.0, 1.0, 1.0), relativeHeight);
			}

			float3 evaluateLighting(float3 startPos, float3 weatherData, float cosTheta)
			{
				float3 pos = startPos;
				float3 dir = _WorldSpaceLightPos0;
				float stepSize = _LightStepSize * 20;

				float coneRadius = 5.0;
				float3 conePos;

				//light accumulation variables				
				float density = 0;

				//sample light density along cone
				for (int i = 0; i < LIGHTSTEPS; i++)
				{
					conePos = pos + coneRadius * random_vectors[i] * (1 + 1);

					if (_Shaping) 
					{
						density += getCloudDensity(conePos, weatherData) * _Density;
					}
					else 
					{
						density = getCoverage(conePos.xz).r;
						break;
					}
					pos += dir * stepSize;
				}

				density = saturate(density);
				
				float energy = exp(-density) * hgPhase(0.6, cosTheta);   //NUBIS slide 75
				//float energy = max(exp(-density), (exp(-density * 0.25), 0.7));  //NUBIS slide 85

				//energy = max(hgPhase(0.6, cosTheta), silver_intensity * hgPhase(0.99 - silver_spread, cosTheta));  //NUBIS slide 80

				return _Color.rgb * energy * _Scattering.x;
			}

			float4 raymarch(float3 cloudStart, float3 cloudEnd, float3 light)
			{				
				//initialize raymarch position and march vectors
				float3 pos = cloudStart;
				float3 rayDelta = cloudEnd - cloudStart;
				float3 ray = normalize(rayDelta);	

				float stepSize = length(rayDelta / STEPS);

				//light calculation coefficients
				const float abs_mult = -_Absorption * stepSize;

				//for phase function
				float cosTheta = dot(light, ray);

				//color accumulation variables
				float transmittance = 1;
				float3 color = 0;			
				float density = 0;

				for (int i = 0; i < STEPS; i++)
				{
					float3 weatherData = getCoverage(pos.xz);
					float cloudDensity;

					//evaluate density at current position				
					if (_Shaping)
					{
						cloudDensity = getCloudDensity(pos, weatherData);  
					}
					else 
					{
						cloudDensity = weatherData.r;
					}

					if (cloudDensity > 0.0)
					{
						//evaluate trasmittance
						density += cloudDensity;
						transmittance *= beerLambert(cloudDensity, abs_mult);
					}
					
					//exit early if zero transmittance reached
					if (transmittance < 1e-6)
					{
						//return (1, 0, 0, 1);
						break;
					}
				
					if (density > 0.0 && _Lighting)
					{
						color = color * (1.0 - transmittance) + evaluateLighting(pos, weatherData, cosTheta) * transmittance;
					}		

					if (!_Shaping)
						break;

					//increment the step
					pos += ray * stepSize;
				}			

				float opacity = 1.0 - transmittance;

				//final cloud color		
				if (_Lighting) {
					return float4(color * opacity, opacity);
				}
				else {
					return float4(0.0, 0.0, 0.0, opacity);						//transmittance only
				}
			}
					
			ENDCG
		}
	}
	Fallback Off
}
