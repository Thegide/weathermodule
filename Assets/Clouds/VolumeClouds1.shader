﻿/*Volumetric cloud shader (c) Maelstrom Studios 2018
References:
Hogfeldt, R. 2016
Hillaire, S. 2016
Palenik, J. 2016
*/

Shader "Volume/Clouds" {
	Properties{
		_WeatherTex("Weather Texture", 2D) = "" {}
		_CloudTex("Cloud Shape Texture", 3D) = "" {}
		_CloudDetail("Cloud Detail Texture", 3D) = "" {}

		_Color("Ambient Light Color", Color) = (1, 1, 1, 1)

		_EarthRadius("Earth Radius", Float) = 6000					//metres
		_CloudStartHeight("Atmosphere Start Height", Float) = 200
		_CloudEndHeight("Atmosphere End Height", Float) = 400
		_CloudLayer("Cloud Layer", Int) = 0

		_Steps("Raymarch Steps", Range(1,256)) = 64
		_Gain("Noise Gain", Range(0.1, 10.0)) = 0.7
		_Scale("Noise Scale", Range(1,512)) = 512

		[Toggle]_Lighting("Lighting Enabled", Range(0,1)) = 1
		_Absorption("Absorption coefficient", Float) = 0.04343
		_Scattering("Scattering coefficient", Float) = 0.7
		_LightStepSize("Light Step Size", Float) = 1
		_Powder("Powder", Float) = 1
		
		//_Blend("Perlin-Worley Blend", Range(0.0, 1.0)) = 0.5

		_Speed("Animation Speed", Float) = 50
		
	}
		
	SubShader {
		Tags{"LightMode" = "Deferred"}
		Cull Off ZWrite Off ZTest Always

		Pass {

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc" //helper functions and macros
			#include "Lighting.cginc"
			#include "VolumeMath.cginc"
			
			//volume projection
			uniform float4x4 _FrustumCornersES;
			uniform float4x4 _CameraInvViewMatrix;
			uniform float4 _MainTex_TexelSize;

			//textures
			uniform sampler2D _MainTex;					//post-processing input
			uniform sampler2D _CameraDepthTexture;		//sky culling
			uniform sampler2D _WeatherTex;
			uniform sampler3D _CloudTex;
			uniform sampler3D _CloudDetail;

			float4 _Color;
			float _Steps;
			float _Lighting;
			float _Absorption;
			float _Scattering;
			float _LightStepSize;
			float _Powder;
			float _Blend;

			float _EarthRadius;
			float _CloudStartHeight;
			float _CloudEndHeight;
			int _CloudLayer;
			
			float _Gain;
			float _Scale;
			float _Speed;

			#define kInnerRadius (_EarthRadius + _CloudStartHeight)
			#define kOuterRadius (_EarthRadius + _CloudEndHeight)
			#define kInnerRadius2 (kInnerRadius * kInnerRadius)
			#define kOuterRadius2 (kOuterRadius * kOuterRadius)
			#define kEarthCenter float3(0, -_EarthRadius, 0)

			#define BELOW_CLOUDS 0
			#define IN_CLOUDS 1
			#define ABOVE_CLOUDS 2

			#define STEPS _Steps
			#define LIGHTSTEPS 6
			
			//structs
			struct appdata 
			{
				float4 vertex : POSITION;
				float3 uv : TEXCOORD0;
			};

			struct v2f 
			{
				float4 pos : SV_POSITION;		//clip space
				float3 uv : TEXCOORD0;
				float3 wPos : TEXCOORD1;		//world position
				float3 ray : TEXCOORD2;
			};
			
			//function declarations			
			float4 raymarch(float3 startPos, float3 endPos, float3 lightRay);
			float getDensity(float3 pos);

			//vertex program
			v2f vert(appdata v)
			{
				v2f o;

				half index = v.vertex.z;
				v.vertex.z = 0.1;

				o.pos = UnityObjectToClipPos(v.vertex);  //maps vertices to view space
				o.uv = v.uv;

				#if UNITY_UV_STARTS_AT_TOP									//prevent flipped textures on some DX machines
				if (_MainTex_TexelSize.y < 0)
					o.uv.y = 1 - o.uv.y;
				#endif
				
				o.wPos = mul((float3x3)unity_ObjectToWorld, v.vertex.xyz); //get world position				
				o.ray = _FrustumCornersES[(int)index].xyz;
				o.ray = normalize(mul(_CameraInvViewMatrix, o.ray));
				return o;
			}

			//fragment program
			fixed4 frag(v2f i) : SV_Target
			{		
				float3 lightRay = normalize(_WorldSpaceLightPos0 - _WorldSpaceCameraPos);		//direction of light

				float3 entry;
				float3 exit;

				//below cloud layer
				if (_CloudLayer == BELOW_CLOUDS)
				{
					entry = raySphereIntersect(_WorldSpaceCameraPos, i.ray, kEarthCenter, kInnerRadius2);
					exit = raySphereIntersect(_WorldSpaceCameraPos, i.ray, kEarthCenter, kOuterRadius2);
				}
				else if (_CloudLayer == ABOVE_CLOUDS)
				{
					entry = raySphereIntersect(_WorldSpaceCameraPos, i.ray, kEarthCenter, kOuterRadius2);
					exit = raySphereIntersect(_WorldSpaceCameraPos, i.ray, kEarthCenter, kInnerRadius2);
				}
				else
				{
					entry = _WorldSpaceCameraPos;
					exit = raySphereIntersect(_WorldSpaceCameraPos, i.ray, kEarthCenter, kInnerRadius2);
					if (distance(entry, exit) > 30000)
					{
						exit = _WorldSpaceCameraPos + i.ray * 30000;
					}
				}

				if (entry.y < 0)
				{
					return tex2D(_MainTex, i.uv);
				}

				//apply effect to skybox only by occlusion
				float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv);				
				if (depth < .00001) {
					fixed4 cloudColor = raymarch(entry, exit, lightRay);				//ray march from the vertex back to the camera
					
					//debug rays - paint x-z axes
					//if (i.ray.x < 0.001 && i.ray.x > -0.001) { cloudColor = fixed4(1, 0, 0, 1); }
					//if (i.ray.y < 0.001 && i.ray.y > -0.001) { cloudColor = fixed4(0, 1, 0, 1); }
					//if (i.ray.z < 0.001 && i.ray.z > -0.001) { cloudColor = fixed4(0, 0, 1, 1); }

					//debug rays - paint x-z axes
					//if (entry.x % 100 < 0.5 && entry.x % 100 > -0.5) { cloudColor = fixed4(1, 0, 0, 1); }
					//if (entry.y % 100 < 0.5 && entry.y % 100 > -0.5) { cloudColor = fixed4(0, 1, 0, 1); }
					//if (entry.z % 100 < 0.5 && entry.z % 100 > -0.5) { cloudColor = fixed4(0, 0, 1, 1); }

					//debug rays - paint x-z axes
					//if (exit.x % 100 < 0.5 && exit.x % 100 > -0.5) { cloudColor = fixed4(1, 0, 0, 1); }
					//if (exit.y % 100 < 0.5 && exit.y % 100 > -0.5) { cloudColor = fixed4(0, 1, 0, 1); }
					//if (exit.z % 100 < 0.5 && exit.z % 100 > -0.5) { cloudColor = fixed4(0, 0, 1, 1); }
					
					return lerp(tex2D(_MainTex, i.uv), cloudColor, cloudColor.a);		//blend with source render texture
				}				
				return tex2D(_MainTex, i.uv);
			}

			float4 evaluateLighting(float3 startPos, float cosTheta)
			{
				float3 pos = startPos;
				float3 dir = _WorldSpaceLightPos0;
				float stepSize = _LightStepSize * 500;

				//light accumulation variables
				float transmittance = 1;
				float density = 0;

				for (int i = 0; i < LIGHTSTEPS; i++)
				{
					density += getDensity(pos);
					pos += dir * stepSize;
				}

				transmittance = exp(-density);

				return _Color * transmittance * miePhase(cosTheta) * _Scattering;
			}
			
			
			float4 raymarch(float3 cloudStart, float3 cloudEnd, float3 light)
			{				
				//initialize raymarch position and march vectors
				float3 pos = cloudStart;
				float3 rayDelta = cloudEnd - cloudStart;
				float3 ray = normalize(rayDelta);
				float3 rayStep = rayDelta / STEPS;				
				float stepSize = length(rayStep);

				//light calculation coefficients
				const float abs_mult = -_Absorption * stepSize;

				//for phase function
				float cosTheta = dot(light, ray);

				//color accumulation variables
				float transmittance = 1;
				float3 color = 0;

				for (int i = 0; i < STEPS; i++)
				{
					//increment the step
					pos += ray * stepSize;

					//evaluate density at current position
					float density = getDensity(pos);// *HeightSignal(rayOrigin, pos, entry);

					//evaluate trasmittance
					float T = exp(abs_mult * density);  //Beer-Lambert law							
					transmittance *= T;
					if (transmittance < 1e-6)
						break;

					//evaluate lighting via single scattering for all samples with density greater than zero	
					if (density > 0)
					{					
						color = color * (1 - transmittance) + evaluateLighting(pos, cosTheta) * transmittance;
					}
				}			

				float opacity = 1 - transmittance;

				//final cloud color		
				color *= opacity;

				if (_Lighting) {
					return float4(color, opacity);
				}
				else {
					return float4(0, 0, 0, opacity);						//transmittance only
				}
			}

			float erode(float x, float e)
			{
				return max(1 - (1 - x) / e, 0);
			}
			
			float getDensity(float3 pos)
			{
				pos.x += _Time.y * _Speed;
				pos.y += sin(_Time.x * _Speed * (pos.x + pos.z) * 0.01) * 10;

				float4 noise = tex3Dlod(_CloudTex, fixed4(pos / (_Scale * 10), 1.0).rbga);
			
				float density = noise.r * _Gain;
		
				return density;
			}

				
			ENDCG
		}
	}
	Fallback Off
}
